FROM python:3

ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY dist/spi_em_class*tar.gz .
RUN pip install spi_em_class*

WORKDIR /work